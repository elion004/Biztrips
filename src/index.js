import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import {BrowserRouter} from "react-router-dom";
import {ErrorBoundary} from 'react-error-boundary';
import ErrorFallback from './ErrorFallback';

ReactDOM.render(
    <ErrorBoundary FallbackComponent={ErrorFallback}>
        <BrowserRouter>
            <App/>
        </BrowserRouter>
    </ErrorBoundary>,
    document.getElementById("root")
);

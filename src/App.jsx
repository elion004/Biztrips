import "./App.css";
import 'bootstrap-dark-5/dist/css/bootstrap-dark.min.css';
import "bootstrap/dist/css/bootstrap.min.css";
import StartPage from "./pages/StartPage";
import {Container} from "react-bootstrap";
import {Route, Routes} from "react-router-dom";
import HotelsPage from "./pages/HotelsPage";
import Navigation from "./Navigation";
import AddHotelPage from "./pages/AddHotelPage";
import AddPersonPage from "./pages/AddPersonPage";
import EditHotelPage from "./pages/EditHotelPage";
import DetailPage from "./pages/DetailPage";

function App() {
    return (
        <>
            <Navigation/>
            <Container>
                    <Routes>
                        <Route path="/" element={<StartPage/>}/>
                        <Route path="/hotels" element={<HotelsPage/>}/>
                        <Route path="/hotels/hotel/:id" element={<DetailPage/>}/>
                        <Route path="/addHotel" element={<AddHotelPage/>}/>
                        <Route path="/editHotel/:id" element={<EditHotelPage/>}/>
                        <Route path="/hotels/:id/addPerson" element={<AddPersonPage/>}/>
                    </Routes>
            </Container>
        </>
);
}

export default App;

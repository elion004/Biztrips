const baseUrl = process.env.REACT_APP_API_BASE_URL;

export async function getHotels() {
    const response = await fetch(baseUrl + "http://localhost:3001/Hotels");
    if (response.ok){
        const data = await response.json()
        return data;
    }
    throw response;
}

export async function addHotel(hotel) {
    return fetch(baseUrl + "Hotels", {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(hotel),
    });
}

export async function getHotel(id) {
    const response = await fetch(baseUrl + "Hotels/" + id);
    if (response.ok){
        const json = await response.json();
        return json;
    }
    throw response;
}

export async function updateHotel(hotel, id) {
    return fetch(baseUrl + "Hotels/" + id, {
        method: "PUT",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(hotel),
    });
}

export async function deleteHotel(id) {
    return fetch(baseUrl + "Hotels/" + id, {
        method: "DELETE",
        headers: {
            "Content-Type": "application/json",
        }
    });
}



import {addHotel, getHotel, getHotels, updateHotel} from "./hotelsService";

beforeEach(() => {
    fetch.resetMocks();
});

it("fetch data by id", async () => {
    fetch.mockResponseOnce(JSON.stringify({
        id: 1,
        Name: "The Ritz",
        Country: "England",
        Address: "150 Piccadilly, St. James's, London W1J 9BR",
        Guests: [
            {
                "id": "688f24-53ad-66fb-f860-e00f2da1bed1",
                "Name": "Elion",
                "LastName": "Bajrami",
                "Department": "IT"
            }
        ]
    }))
    const data = await getHotel(1);

    expect(data).toEqual({
        id: 1,
        Name: "The Ritz",
        Country: "England",
        Address: "150 Piccadilly, St. James's, London W1J 9BR",
        Guests: [
            {
                "id": "688f24-53ad-66fb-f860-e00f2da1bed1",
                "Name": "Elion",
                "LastName": "Bajrami",
                "Department": "IT"
            }
        ]
    })
    expect(fetch).toHaveBeenCalledTimes(1);
})

it("fetch data", async () => {
    fetch.mockResponseOnce(JSON.stringify({
        id: 1,
        Name: "The Ritz",
        Country: "England",
        Address: "150 Piccadilly, St. James's, London W1J 9BR",
        Guests: [
            {
                "id": "688f24-53ad-66fb-f860-e00f2da1bed1",
                "Name": "Elion",
                "LastName": "Bajrami",
                "Department": "IT"
            }
        ]
    }))
    const data = await getHotels();

    expect(data).toEqual({
        id: 1,
        Name: "The Ritz",
        Country: "England",
        Address: "150 Piccadilly, St. James's, London W1J 9BR",
        Guests: [
            {
                "id": "688f24-53ad-66fb-f860-e00f2da1bed1",
                "Name": "Elion",
                "LastName": "Bajrami",
                "Department": "IT"
            }
        ]
    })
    expect(fetch).toHaveBeenCalledTimes(1);
})

it("fetch post data", async () => {
    fetch.mockResponseOnce(JSON.stringify({
        id: 1,
        Name: "The Ritz",
        Country: "England",
        Address: "150 Piccadilly, St. James's, London W1J 9BR",
        Guests: [
            {
                "id": "688f24-53ad-66fb-f860-e00f2da1bed1",
                "Name": "Elion",
                "LastName": "Bajrami",
                "Department": "IT"
            }
        ]
    }))
    const data = await addHotel({
        Name: "Test",
        Country: "England",
        Address: "150 Piccadilly, St. James's, London W1J 9BR",
        Guests: [
            {
                "id": "688f24-53ad-66fb-f860-e00f2da1bed1",
                "Name": "Elion",
                "LastName": "Bajrami",
                "Department": "IT"
            }
        ]
    });

    expect(fetch).toHaveBeenCalledTimes(1);
})

it("fetch update data", async () => {
    fetch.mockResponseOnce(JSON.stringify({
        id: 4,
        Name: "The Ritz",
        Country: "England",
        Address: "150 Piccadilly, St. James's, London W1J 9BR",
        Guests: [
            {
                "id": "688f24-53ad-66fb-f860-e00f2da1bed1",
                "Name": "Elion",
                "LastName": "Bajrami",
                "Department": "IT"
            }
        ]
    }))
    const data = await updateHotel({
        Name: "Test",
        Country: "England",
        Address: "150 Piccadilly, St. James's, London W1J 9BR",
        Guests: [
            {
                "id": "688f24-53ad-66fb-f860-e00f2da1bed1",
                "Name": "Elion",
                "LastName": "Bajrami",
                "Department": "IT"
            }
        ]
    }, 4);

    //expect(data.response.status).toEqual(200);

    expect(fetch).toHaveBeenCalledTimes(1);
})
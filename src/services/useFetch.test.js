import {useFetchForTest} from "./useFetch";

beforeEach(() => {
    fetch.resetMocks();
});

it("fetch data with useFetch", async () => {
    fetch.mockResponseOnce(JSON.stringify({
        id: 1,
        Name: "The Ritz",
        Country: "England",
        Address: "150 Piccadilly, St. James's, London W1J 9BR",
        Guests: [
            {
                "id": "688f24-53ad-66fb-f860-e00f2da1bed1",
                "Name": "Elion",
                "LastName": "Bajrami",
                "Department": "IT"
            }
        ]
    }))
    const data = await useFetchForTest("Hotels");

    expect(data).toEqual({
        id: 1,
        Name: "The Ritz",
        Country: "England",
        Address: "150 Piccadilly, St. James's, London W1J 9BR",
        Guests: [
            {
                "id": "688f24-53ad-66fb-f860-e00f2da1bed1",
                "Name": "Elion",
                "LastName": "Bajrami",
                "Department": "IT"
            }
        ]
    })
    expect(fetch).toHaveBeenCalledTimes(1);
})
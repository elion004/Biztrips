import {Badge, Button, Container, Image, ListGroup, Navbar, Offcanvas} from 'react-bootstrap';
import {useEffect, useState} from "react";
import 'bootstrap-dark-5/dist/css/bootstrap-dark.min.css';
import {LinkContainer} from 'react-router-bootstrap';
import {Link} from "react-router-dom";

export default function Navigation() {

    const [show, setShow] = useState(false);

    useEffect(() =>{
        if(window.location.href !== "http://localhost:3000/addHotel"){
            setShow(true)
        }
        else {
            setShow(false)
        }
    },[window.location.href])

    return (
        <Navbar bg="dark">
            <Container>
                <LinkContainer to="/">
                    <Navbar.Brand style={{textDecoration: "none", color:"white"}}>
                        <h4 style={{color:"white"}}>Biztrips</h4>
                    </Navbar.Brand>
                </LinkContainer>

                {show === true ?
                    <Navbar.Text>
                        <Link to="/addHotel">
                            <Button variant={"outline-light"}>Add new Hotel</Button>
                        </Link>
                    </Navbar.Text>
                    :
                    <h1/>
                }
            </Container>
        </Navbar>
    );
}

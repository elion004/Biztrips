import {Button, Col, Form, Row} from "react-bootstrap";
import {useEffect, useState} from "react";
import {Link, Outlet, useParams} from "react-router-dom";
import {getHotel, updateHotel} from "../services/hotelsService";
import uuid from 'react-uuid'

export default function AddPersonPage() {
    const [hotel, setHotel] = useState();

    const [name, setName] = useState();
    const [lastName, setLastName] = useState();
    const [department, setDepartment] = useState();

    let params = useParams();

    function returnGuest(id,name, lastName, department) {
        return {
            id: id,
            Name: name,
            LastName: lastName,
            Department: department,
        }
    }

    function handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.id;

        switch (name) {
            case "name":
                setName(value);
                break;
            case "lastName":
                setLastName(value);
                break;
            case "department":
                setDepartment(value);
                break;
        }
    }

    useEffect(() => {
        getHotel(params.id)
            .then(r => {
                setHotel(r)
                console.log(r)
            })
    }, [])

    function addPersonFunction() {
        hotel.Guests.push(returnGuest(uuid(), name, lastName, department))
        updateHotel(hotel, params.id)
    }

    return (
        <>
            <div style={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                height: '100vh',
                flexDirection: "column"
            }}>
                <h1 style={{color:"white"}}>Add Person to Hotel</h1>
                <div style={{border: "solid white 1px", padding: "50px", borderRadius: "10px"}}>
                    <Form>
                        <Row>
                            <Col>
                                <Form.Group className="mb-3" controlId="name" onChange={handleInputChange}>
                                    <Form.Control type="name" placeholder="name"/>
                                    <Form.Text className="text-muted">
                                        First name
                                    </Form.Text>
                                </Form.Group>
                            </Col>
                            <Col>
                                <Form.Group className="mb-3" controlId="lastName" onChange={handleInputChange}>
                                    <Form.Control type="name" placeholder="Last name"/>
                                    <Form.Text className="text-muted">
                                        Last name
                                    </Form.Text>
                                </Form.Group>
                            </Col>
                        </Row>

                        <Form.Group className="mb-3" controlId="department" onChange={handleInputChange}>
                            <Form.Control type="department" placeholder="department"/>
                            <Form.Text className="text-muted">
                                The department for example "IT"
                            </Form.Text>
                        </Form.Group>

                        <div style={{
                            display: 'flex',
                            justifyContent: 'center',
                            alignItems: 'center',
                            flexDirection: "column"
                        }}>
                            <Link to={"/Hotels"}>
                                <Button variant="outline-light" type="submit" onClick={addPersonFunction}>
                                    Submit
                                </Button>
                            </Link>
                        </div>
                    </Form>
                    <Outlet/>
                </div>
            </div>
        </>
    );
}



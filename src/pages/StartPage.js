import {Button} from "react-bootstrap";
import {Link, Outlet} from "react-router-dom";

export default function StartPage() {

    return (
        <>
            <div style={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                height: '100vh',
                flexDirection: "column"
            }}>
                <h1 style={{color:"white"}}>Hotels Management for Company XY</h1>
                <br/>
                <Link to="/hotels">
                    <Button variant="outline-primary">Start</Button>
                </Link>
                <Outlet/>
            </div>
        </>
    );
}

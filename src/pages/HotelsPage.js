import {Button, Card, Col, Row} from "react-bootstrap";
import useFetch from "../services/useFetch";
import Spinner from "../Spinner";
import {Link, Outlet} from "react-router-dom";
import {deleteHotel} from "../services/hotelsService";

export default function HotelsPage() {

    function randomImage(search) {
        return "https://source.unsplash.com/300x300/?" + search;
    }

    const {data: hotels, loading: loadingHotel, error: errorHotel} = useFetch(
        "Hotels"
    );

    return (
        <>
            <div style={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                height: '100vh',
                flexDirection: "column"
            }}>
                <h1 style={{color:"white"}}>Hotels</h1>
                {loadingHotel === true ? <Spinner/> :
                    <Row>
                        {Object.entries(hotels)
                            .map(([key, item]) =>
                                <Col>
                                    <Card key={key}>
                                        <Card.Img height={400} src={randomImage(item.Name)}>
                                        </Card.Img>
                                        <Card.Body>
                                            <Card.Title>{item.Name}</Card.Title>
                                            <Card.Text>
                                                {item.Country}
                                            </Card.Text>
                                            <Link to={"/Hotels/" + item.id + "/addPerson"}>
                                                <Button variant="outline-dark" type="submit"
                                                        style={{marginBottom: "10px"}}>
                                                    Add Employee to Hotel
                                                </Button>
                                            </Link>
                                            <br/>
                                            <Link to={"/editHotel/" + item.id}>
                                                <Button variant="outline-dark" style={{marginRight: "10px"}}>Edit
                                                    Hotel</Button>
                                            </Link>
                                            <Link to={"/hotels/hotel/" + item.id}>
                                                <Button variant="outline-dark">More</Button>
                                            </Link>
                                            <Link to={"/hotels"}>
                                                <Button variant="outline-danger" style={{marginLeft:"10px"}} onClick={() => deleteHotel(item.id)}>Delete</Button>
                                            </Link>
                                        </Card.Body>
                                    </Card>
                                </Col>
                            )
                        }
                        <Outlet/>
                    </Row>
                }
            </div>
        </>
    );
}



import {Button, Card, Col, Form, ListGroup, Row} from "react-bootstrap";
import {useEffect, useState} from "react";
import {Link, useParams} from "react-router-dom";
import {addHotel, deleteHotel, getHotel} from "../services/hotelsService";

export default function DetailPage() {
    const [name, setName] = useState();
    const [country, setCountry] = useState();
    const [address, setAddress] = useState();
    const [guests, setGuests] = useState();

    let params = useParams();

    useEffect(() => {
        getHotel(params.id).then(r => {
            setName(r.Name)
            setAddress(r.Address)
            setCountry(r.Country)
            setGuests(r.Guests)
        })
    }, [])

    return (
        <>
            <div style={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                height: '100vh',
                flexDirection: "column"
            }}>
                <h1 style={{color: "white"}}>{name}</h1>

                <br/>

                <h2 style={{color: "white"}}>
                    Country:
                </h2>

                <br/>

                <p style={{color: "white", fontStyle:"italic"}}>
                    {country}
                </p>

                <br/>

                <h2 style={{color: "white"}}>
                    Address:
                </h2>

                <br/>

                <p style={{color: "white", fontStyle:"italic"}}>
                    {address}
                </p>

                <br/>

                <h2 style={{color: "white"}}>
                    Employees:
                </h2>

                {guests != null ?
                    <ListGroup>
                        {Object.entries(guests)
                            .map(([key, item]) =>
                                <ListGroup.Item
                                    style={{minWidth: "500px"}}>{item.Name + " " + item.LastName + " " + item.Department}</ListGroup.Item>
                            )
                        }
                    </ListGroup>
                    :
                    <h3>Still None</h3>
                }

            </div>
        </>
    );
}



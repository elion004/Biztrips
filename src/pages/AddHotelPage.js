import {Button, Col, Form, Row} from "react-bootstrap";
import {useState} from "react";
import {Link} from "react-router-dom";
import {addHotel} from "../services/hotelsService";

export default function AddHotelPage() {
    const [name, setName] = useState();
    const [country, setCountry] = useState();
    const [address, setAddress] = useState();

    function hotel(name, country, address) {
        return {
            Name: name,
            Country: country,
            Address: address,
            Guests: []
        }
    }

    function handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.id;

        switch (name) {
            case "name":
                setName(value);
                break;
            case "country":
                setCountry(value);
                break;
            case "address":
                setAddress(value);
                break;
        }
    }

    return (
        <>
            <div style={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                height: '100vh',
                flexDirection: "column"
            }}>
                <h1 style={{color:"white"}}>Add Hotel</h1>
                <div style={{border: "solid white 1px", padding: "50px", borderRadius: "10px"}}>
                    <Form>
                        <Form.Group className="mb-3" controlId="name" onChange={handleInputChange}>
                            <Form.Control type="name" placeholder="Enter name"/>
                            <Form.Text className="text-muted">
                                The Hotel Name
                            </Form.Text>
                        </Form.Group>

                        <Row>
                            <Col>
                                <Form.Group className="mb-3" controlId="country" onChange={handleInputChange}>
                                    <Form.Control type="text" placeholder="country"/>
                                    <Form.Text className="text-muted">
                                        For example Switzerland
                                    </Form.Text>
                                </Form.Group>
                            </Col>
                            <Col>
                                <Form.Group className="mb-3" controlId="address" onChange={handleInputChange}>
                                    <Form.Control type="text" placeholder="address"/>
                                    <Form.Text className="text-muted">
                                        Please a valid address
                                    </Form.Text>
                                </Form.Group>
                            </Col>
                        </Row>

                        <div style={{
                            display: 'flex',
                            justifyContent: 'center',
                            alignItems: 'center',
                            flexDirection: "column"
                        }}>
                            <Link to={"/Hotels"}>
                                <Button variant="outline-light" type="submit" onClick={() => addHotel(hotel(name, country, address))}>
                                    Submit
                                </Button>
                            </Link>
                        </div>
                    </Form>
                </div>
            </div>
        </>
    );
}


